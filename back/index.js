'use strict';

const server = require('./server.js');
const config = require('./config.js');

server.listen(config.port);
