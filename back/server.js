'use strict';

const debug = require('debug')('Application');

const Koa = require('koa');
const logger = require('koa-logger');
const body = require('koa-bodyparser');

const app = module.exports = new Koa();

const data = {
	value: 'asdf',
};

app.use(logger());
app.use(body());
app.use(async (ctx) => {
	if (ctx.method === 'GET') {
		ctx.body = data.value;
	} else if (ctx.method === 'POST') {
		ctx.body = data.value = ctx.request.body.value;
	}
});
