module gitlab.com/aelmalinka/junk-website/grpc

go 1.15

require (
	github.com/golang/protobuf v1.4.2
	gitlab.com/aelmalinka/junk-website/grpc/proto v0.0.0-20201009060559-dd278633fb75
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
)
