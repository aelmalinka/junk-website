package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"google.golang.org/grpc"

	pb "gitlab.com/aelmalinka/junk-website/grpc/proto"
)

var (
	addr	= flag.String("addr", "localhost:3002", "The port to listen on")
)

type client struct {
	conn	*grpc.ClientConn
	client	pb.HelloServiceClient
}

func NewClient(addr string) (*client, error) {
	var err error
	ret := new(client)

	ret.conn, err = grpc.Dial(addr, grpc.WithInsecure())

	if err != nil {
		return nil, err
	} else {
		ret.client = pb.NewHelloServiceClient(ret.conn)
		return ret, nil
	}
}

func (c *client) Close() (error) {
	return c.conn.Close()
}

func (c *client) Say(what string) {
	req := &pb.Hello{}
	req.What = what

	res, err := c.client.Say(context.Background(), req)

	if err != nil {
		log.Fatalf("Say failed: %v", err)
	} else {
		fmt.Println(res.What)
	}
}

func main() {
	flag.Parse()

	client, err := NewClient(*addr)
	if err != nil {
		log.Fatalf("Failed to dial: %v", err)
	}
	defer client.Close()

	buf := bufio.NewReader(os.Stdin)

	// 2020-10-09 AMR TODO: keeps looping
	for err == nil {
		d, err := buf.ReadString('\n')

		if len(d) != 0 {
			go client.Say(d);
		}

		if err != nil && err != io.EOF {
			log.Fatalf("Read failed: %v", err)
		}
	}
}
