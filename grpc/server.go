package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	pb "gitlab.com/aelmalinka/junk-website/grpc/proto"
)

type helloServer struct {
	pb.UnimplementedHelloServiceServer

	last string
}

func newServer() *helloServer {
	ret := &helloServer{}

	ret.last = "to be set"

	return ret
}

func (s *helloServer) Say(ctx context.Context, req *pb.Hello) (*pb.Hello, error) {
	res := new(pb.Hello)

	res.What = s.last
	s.last = req.What

	return res, nil
}

var (
	port	= flag.Uint("port", 9090, "The port to listen on")
)

func main() {
	flag.Parse()

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	serv := grpc.NewServer()
	pb.RegisterHelloServiceServer(serv, newServer())

	log.Printf("Listening on %d\n", *port)
	serv.Serve(lis)
}
