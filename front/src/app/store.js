import { configureStore } from '@reduxjs/toolkit';
import back from '../features/back/slice';
import grpc from '../features/grpc/slice';

export default configureStore({
	reducer: {
		back,
		grpc,
	},
});
