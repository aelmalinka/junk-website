import {
	createAsyncThunk,
	createSlice,
} from '@reduxjs/toolkit';

// 2020-10-06 AMR TODO: configurable
export const getBack = createAsyncThunk('back/get', async () => {
	const res = await window.fetch('/api/', { method: 'GET', });
	return await res.text();
});

export const setBack = createAsyncThunk('back/set', async (what) => {
	const res = await window.fetch('/api/', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			value: what,
		}),
	});
	return await res.text();
});

export const slice = createSlice({
	name: 'back',
	initialState: {
		value: 'Loading...',
		status: 'initial',
	},
	extraReducers: {
		[getBack.pending]: (state, action) => {
			state.status = 'pending';
		},
		[getBack.fulfilled]: (state, action) => {
			state.status = 'complete';
			state.value = action.payload;
		},
		[getBack.rejected]: (state, action) => {
			state.status = 'failed';
			state.value = action.error;
		},
		[setBack.pending]: (state, action) => {
			state.status = 'updating';
		},
		[setBack.fulfilled]: (state, action) => {
			state.status = 'complete';
			state.value = action.payload;
		},
		[setBack.rejected]: (state, action) => {
			state.status = 'failed';
			state.value = action.error;
		},
	},
});

export default slice.reducer;

export const selectBack = (state) => state.back.value;
export const selectBackStatus = (state) => state.back.status;
