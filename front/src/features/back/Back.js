import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import {
	getBack,
	setBack,
	selectBack,
	selectBackStatus,
} from './slice';

export const Back = () => {
	const dispatch = useDispatch();

	const back = useSelector(selectBack);
	const status = useSelector(selectBackStatus);

	const message = typeof back === 'string' ? back : back?.message;
	const [value, setValue] = useState('value');

	useEffect(() => {
		if (status === 'initial')
			dispatch(getBack());
	}, [status, dispatch]);

	const canSave = value !== 'value';
	const onValueChange = e => setValue(e.target.value);
	const onSubmit = async () => {
		try {
			setValue('value');
			const res = await dispatch(setBack(value));
			unwrapResult(res);

			setValue('value');
		} finally {
			setValue(value);
		}
	};

	return (
		<div>
			<div>{message}</div>
			<form>
				<input type='text'
					onChange={onValueChange}
					value={value}
				/>
				<button type='button'
					onClick={onSubmit}
					disabled={!canSave}
				>
					Submit
				</button>
			</form>
		</div>
	)
};
