import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import {
	sayGrpc,
	selectSay,
	selectSayStatus,
} from './slice';

export const Grpc = () => {
	const dispatch = useDispatch();

	const start = useSelector(selectSay);
	const status = useSelector(selectSayStatus);

	const [last, setLast] = useState(start);
	const [value, setValue] = useState('say?');

	const say = (what) => dispatch(sayGrpc(what))
		.then(unwrapResult)
		.then(res => setLast(res))
		.catch(err => setLast(`error: ${err.message}`));

	useEffect(() => {
		if(status === 'initial')
			say('Hello');
	});

	const canSave = status === 'completed';
	const onValueChange = e => setValue(e.target.value);
	const onSubmit = () => {
		say(value);
	};

	return (
		<div>
			<div>{last}</div>
			<form>
				<input type='text'
					onChange={onValueChange}
					value={value}
				/>
				<button type='button'
					onClick={onSubmit}
					disabled={!canSave}
				>Submit</button>
			</form>
		</div>
	);
};
