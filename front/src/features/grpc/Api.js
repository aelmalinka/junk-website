import { Hello } from '../../pb/hello_pb'
import { HelloServiceClient } from '../../pb/hello_grpc_web_pb';

// 2020-10-10 AMR FIXME: this is a problem
const client = new HelloServiceClient('http://localhost/grpc');

export const Say = (what) => {
	const req = new Hello();

	req.setWhat(what);

	// 2020-10-11 AMR FIXME: swap to promise grpc api
	return new Promise((resolve, reject) => {
		client.say(req, {}, (err, res) => {
			if(err) reject(err);

			resolve(res.getWhat());
		});
	});
};
