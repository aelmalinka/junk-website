import {
	createAsyncThunk,
	createSlice,
} from '@reduxjs/toolkit';

import { Say } from './Api';

export const sayGrpc = createAsyncThunk('grpc/say', async (what) => {
	return await Say(what);
});

export const slice = createSlice({
	name: 'grpc',
	initialState: {
		value: 'unknown',
		status: 'initial',
	},
	extraReducers: {
		// 2020-10-10 AMR TODO: seems like an abstractable pattern
		[sayGrpc.pending]: (state, action) => {
			state.status = 'pending';
		},
		[sayGrpc.fulfilled]: (state, action) => {
			state.status = 'completed';
			state.value = action.payload;
		},
		[sayGrpc.rejected]: (state, action) => {
			state.status = 'error';
			state.value = action.error;
		},
	},
});

export default slice.reducer;

export const selectSay = (state) => state.grpc.value;
export const selectSayStatus = (state) => state.grpc.status;
