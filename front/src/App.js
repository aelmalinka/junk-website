import React from 'react';
import { Back } from './features/back/Back';
import { Grpc } from './features/grpc/Grpc';

function App() {
	return (
		<div>
			<section><Back /></section>
			<section><Grpc /></section>
		</div>
	);
}

export default App;
