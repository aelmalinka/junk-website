FROM node:latest AS protobuild

ARG PB_VERSION=3.12.4
ARG GRPC_WEB_VERSION=1.2.1

WORKDIR /tmp

RUN curl -sSL https://github.com/protocolbuffers/protobuf/releases/download/v$PB_VERSION/protoc-$PB_VERSION-linux-x86_64.zip -o protoc.zip && \
	unzip protoc.zip && \
	mkdir -p /usr/local/bin && \
	cp ./bin/protoc /usr/local/bin/protoc

RUN \
	curl -sSL https://github.com/grpc/grpc-web/releases/download/$GRPC_WEB_VERSION/protoc-gen-grpc-web-$GRPC_WEB_VERSION-linux-x86_64 -o /usr/local/bin/protoc-gen-grpc-web && \
	chmod +x /usr/local/bin/protoc-gen-grpc-web

ADD ./proto proto
RUN mkdir out

RUN for f in proto/*.proto ; do \
		echo "generating ${f}" ; \
		protoc -I=/tmp/include -I=proto $f --js_out=import_style=commonjs:out --grpc-web_out=import_style=commonjs,mode=grpcweb:out ; \
	done

FROM node:latest AS build

WORKDIR /app

COPY package*.json ./
RUN npm ci --production

ENV PATH=/app/node_modules/.bin:$PATH
RUN mkdir -p /app/src/pb
COPY --from=protobuild /tmp/out/ /app/src/pb/
COPY . .

RUN npm run build

FROM nginx:stable

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
